//This is a program that displays the order of precedence used in programming

var addOne, subOne, multOne, divOne, parenthOne, startOne, startTwo, startThree;

//window.alert("Coding also follows the mathematical order of operations.");
startOne = parseInt(window.prompt("enter your 1st number 1-9"));
startTwo = parseInt(window.prompt("enter your 2nd number 1-9"));
startThree = parseInt(window.prompt("enter your 3rd number 1-9"));
addOne = startOne * startTwo + startThree;
subOne = startOne - startTwo - startThree;
multOne = startOne + startTwo * startThree;
divOne = startOne + startTwo / startThree;
parenthOne = (startOne + startTwo) * startThree;
window.alert("correct order of operations for addition of " + startOne + " * " +  startTwo + " + " + startThree + " = " + addOne);
window.alert("correct order of operations for subtraction of " + startOne + " - " +  startTwo + " - " + startThree + " = " + subOne);
window.alert("correct order of operations for multiplication of " + startOne + " + " +  startTwo + " * " + startThree + " = " + multOne);
window.alert("correct order of operations for division of " + startOne + " + " +  startTwo + " / " + startThree + " = " + divOne);
window.alert("correct order of operations when using parenthesis for " + "(" + startOne + " + " +  startTwo + ")" + " * " + startThree + " = " + parenthOne);