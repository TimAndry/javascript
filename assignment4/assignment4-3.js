//This is a program that converts miles into yards, feet, and inches

var distMiles, distYards, distFeet, distInches;

distMiles = window.prompt("Enter any distance in miles and we'll convert it to yards, feet, and inches");
distYards = distMiles * 5280;
distFeet = distYards * 3;
distInches = distFeet * 12;
window.alert("your distance of " + distMiles + " miles converted is " + distYards + " yards, " + distFeet + " feet, or " + distInches + " inches.");