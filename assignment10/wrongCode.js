main();

function main(){
    startQuiz();
    readyQuiz();
    var hundred = getHundred();
    var guess = guessNum(hundred);
    getAnswer(guess);
}

function startQuiz(){
    var start = window.prompt("Number Guessing Quiz! \n \n Are you ready to play? Y/N");
    if(start == "y" || start == "Y" || start == "yes" ||start == "Yes" ||start == "YES"){
        window.alert("Okay. Think of a number 0 - 100 and I will try to guess it");
    }else if(start == "n" || start == "N" || start == "no" || start == "No" || start == "NO"){
        window.alert("Awwwww, I was ready to play! well, have a nice day :D");
    }else{
        window.alert("It was a yes or no question XD");
       startQuiz();
    }
}

function readyQuiz(){
    var ready = window.prompt("Did you think of a number yet? Y/N");
    if(ready == "y" || ready == "Y" || ready == "yes" || ready == "Yes" || ready == "YES"){
        window.alert("Okay. Let's begin! I'm thinking of a guess right now!");
    }else if(ready == "n" || ready == "N" || ready == "no" || ready == "No" || ready == "NO"){
        window.alert("Well think of one fast!");
        readyQuiz();
    }else{
        window.prompt("I don't know what that means...");
        readyQuiz();
    }
}

function getHundred(){
    var hundred = 100;
    return hundred;
}


function guessNum(hundred){
    var guess = Math.floor(Math.random() * hundred);
    return guess;
}


function getAnswer(guess){
    var guessPrompt = window.prompt("is " + guess + " your number? Enter Y if it's right, H if it higher,  or L if it's lower");
    if (guessPrompt == "L" || guessPrompt == "l"){
     while (guess >= 0 && guess <= 100){
         if (guess <= 0){
            window.alert("Ha ha ha...don't cheat. Your number must be between 0 and 100");
            break;
        }else{
            guess = Math.floor(Math.random() * guess);
            getAnswer(guess);
        }
    }
    
  }else if(guessPrompt == "H" || guessPrompt == "h"){
      while (guess <= 100 && guess >= 0){
          if (guess >= 100){window.alert("Ha ha ha...don't cheat. Your number must be between 0 and 100");
          break;
        }else{
            guess = guess + Math.floor(Math.random() * (100-guess));
            getAnswer(guess);
        }
     }
      
  }else if(guessPrompt == "Y" || guessPrompt == "y"){
      window.alert("I guessed it right!");
  }
}
