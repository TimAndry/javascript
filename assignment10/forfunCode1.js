main();

function main(){
    var factorNum = getNum();
    calcFactor(factorNum);
}

function getNum(){
    var factorNum = window.prompt("Enter the factorial you wish to see");
    return factorNum;
}

function calcFactor(factorNum){
    var count = 0;
    var factorUp = 1;
    var total = 1;
    for(count = 0; count <= factorNum; count++){
        total = total * factorUp;
        factorUp ++;
    }
    window.alert(factorUp);
}