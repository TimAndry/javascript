//This is a program that will guess a number that the user inputs between 1-100
main();

//main function that will run the program
function main(){
    startGame();
    var yourNum = setInitial();
    var minNum = setMin();
    var maxNum = setMax();
    var display = playGame(yourNum, minNum, maxNum);
    endGame(display);
}

//message that will display to start the game.
function startGame(){
    window.alert("Think of a number 1-100");
}

//sets the initial number value to 50. This number will be the initial guess. 
function setInitial(){
    var yourNum = 50;
    return yourNum;
}

//sets the minimum number to 0
function setMin(){
    var minNum = 0;
    return minNum;
}

//sets the maximum number to 100
function setMax(){
    var maxNum = 100;
    return maxNum;
}

//uses an algorithm to guess the users number and counts the number of attempts.
function playGame(yourNum, minNum, maxNum){
    var selection = true;
    var count = 0;
    while (selection === true){
        count ++;
        var ask = window.prompt("Is your number " + yourNum + "?\n\ne = equal (or yes), h = higher, l = lower");
        if (ask == "e" || ask == "E"){
            selection = false;
            var display = "I guessed your number in " + count + " tries!";
            return display;
        }else if (ask == "h" || ask == "H"){
            minNum = yourNum;
            yourNum = Math.round((yourNum + maxNum) / 2);
        }else if (ask == "l" || ask == "L"){
           maxNum = yourNum;
           yourNum = Math.round((yourNum + minNum) / 2);
        }else{
            window.prompt("you must enter e,h, or l");
            ask;
        }
    }
}

//displays a message that confirms the guess and show the number of attempts used.
function endGame(display){
    var showResults = display;
    window.alert(showResults);
}