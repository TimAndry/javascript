//This is a program that generates a list of multiplication expressions


main();

//main function that will run the program
function main(){
    var userNum = getNumber();
    var numMultiples = getMultiple();
    var displayOutput = whileLoop(userNum, numMultiples);
    displayResults(displayOutput, userNum, numMultiples);
}

//gets the number of the multiplication table that the user is trying to see
function getNumber(){
    var userNum = parseFloat(window.prompt("Enter the number of the multiplication table you wish to see"));
    return userNum;
}

//gets the number of values that will be displayed in the table
function getMultiple(){
    var numMultiples = parseFloat(window.prompt("Enter the highest number multiplier you wish to see"));
    return numMultiples;
}

//creates the multiplication table using a while loop to calculate and store the results.
function whileLoop(userNum, numMultiples){
    var multiplier = 0;
    var displayOutput = "" ;
    while(multiplier <= numMultiples){
      var multOutput = userNum + " * " + multiplier + " = " + (userNum * multiplier);
      displayOutput = displayOutput + multOutput + "\n";
      multiplier++;
    }
    return displayOutput;
}

//displays the multiplication table
function displayResults(displayOutput, userNum, numMultiples){
  var display = "The multiplication table for " + userNum + " by " + numMultiples + " is:\n \n" + displayOutput;
  window.alert(display);
}

