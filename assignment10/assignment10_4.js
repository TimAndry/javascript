//This is a function that allows a user to guess how many calculations it takes to accurately predict PI to 15 digits. 
main();

//The main function that executes the program
function main(){
    startGame();
    var numTries = getTries();
    var ourPi = setPi();
    var piCalc = countPi(numTries);
    var calcAnswer = getAnswer(ourPi, piCalc);
    var enoughTries = playAgain(calcAnswer, numTries);
    showEnough(enoughTries);
    
}

//displays message to begin the game
function startGame(){
    window.alert("This is a game to test how many iterations are needed to accurately calculate Pi to 15 digits using the Nilakantha series");
}

//Gets the humber of calculations that will be used to predict PI
function getTries(){
    var numTries = window.prompt("How many tries do you think it will take to accurately calculate Pi to 15 digits?");
    return numTries;
}

//sets the comparison value of PI
function setPi(){
    var ourPi = 3.141592653589793;
    return ourPi;
}

//Nilakantha calculation based on the number the user entered that will be used to calculate PI
function countPi(numTries){
    var piCalc = 3;
    var num1 = 2;
    var num2 = 3;
    var num3 = 4;
    var count = 1;
    for(count = 1; count <= numTries; count ++){
        if(count%2 !== 0){
            piCalc = piCalc + (4/(num1 * num2 * num3));
            num1 = num1 + 2;
            num2 = num2 + 2;
            num3 = num3 + 2;
        }else if(count%2 === 0){
            piCalc = piCalc - (4/(num1 * num2 * num3));
            num1 = num1 + 2;
            num2 = num2 + 2;
            num3 = num3 + 2;
        }
    }
    return piCalc;
}

//Asks user if the calculation is accurate and continues or ends the game based on the user input
function getAnswer(ourPi, piCalc, numTries){
    var calcAnswer = window.prompt("Pi to 15 digits is:\n\n " + ourPi + "\n\n" + "Pi at " + numTries + " calculations is \n\n" + piCalc + "\n\n" + "Is the calculation to the first 15 digits equal to the true 15 digit Pi? Y/N\n (Anything else will quit the game)");
    return calcAnswer;
}  

//Continues or quits the game if the calculation is innaccurate
function playAgain(calcAnswer, numTries){
    if (calcAnswer == "y" || calcAnswer == "Y"){
        var enoughTries = "congratulations, " + numTries + " calculations will give you an accurate Pi to 15 digits";
        return enoughTries;
    }else if (calcAnswer == "n" || calcAnswer == "N"){
        var keepPlay = window.prompt("Would you like to guess again? Y/N (anything else will quit the game)");
        if(keepPlay == "y" || keepPlay == "Y"){
            main();
        }else if (calcAnswer == "n" || calcAnswer == "N"){
            window.alert(numTries + " calculations is not enough to calculate Pi to 15 digits. Thanks for playing");
        }else{
            window.alert("have a nice day!");
        }
    }else{
       window.alert("Have a nice day!");
    }
}

//Displays a message that confirms that the user input was enough to calculate PI
function showEnough(enoughTries){
    var resultsEnough = enoughTries;
    window.alert(resultsEnough);
}

