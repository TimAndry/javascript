//This is a program that gets a users fisrt and last name and prints them in the format "lastname, F."

//main function to run the program
main();

function main(){
    var name = getName();
    var index = getIndex(name);
    var first = getFname(index, name);
    var last = getLname(index, name);
    displayName(first, last);
}

//gets the users full name
function getName(){
  var name = input("Enter your first and last name");
  return name;
}

//gets the index of the space between first and last name
function getIndex(name){
    var index = name.indexOf(" ",0);
    return index;
}

//gets first initial from first name of the string
function getFname(index, name){
    var first = name.substring(0, 1);
    return first;
}

//gets last name from the string
function getLname(index, name){
    var last = name.substring(index + 1);
    return last;
}


//displays name in "lastName, F." format and trims whitespace
function displayName(first, last){
    var fullName = last.trim() + ", " + first.trim() + ".";
    output(fullName);
}




function input(text) {
    if (typeof console === 'object') {
        return prompt(text)
    }
    else {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) {
    if (typeof console === 'object') {
        console.log(text);
    } 
    else if (typeof document === 'object') {
        document.write(text);
    } 
    else {
        print(text);
    }
}