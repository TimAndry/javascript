window.alert('How to make a grilled turkey sandwich');
window.alert('Step 1: Acquire the following ingredients. 2 slices of Italian bread, 4 strips of bacon, 1/2 pound of sliced turkey breast, 1/2 Roma tomato, 1/4 red onion, 2 slices of provolone cheese, 1 tbsp mayo or light mayo, 1tbsp olive oil, and 1tbsp of butter');
window.alert('Step 2: Acquire the following cooking utensils. 1 heat resistant cup, 1 medium skillet, 1 vegetable knife, 2 butter knives, 1 spatula, 1 cutting board, 1 plate, 1 fork, and one stove top or hotplate. ');
window.alert('Step 3: Place the tomato and onion on the cutting board');
window.alert('Step 4: Use the vegetable knife to cut the tomato and onion into slices');
window.alert('Step 5: place the medium skillet on the stove top or hotplate and turn heat to 350 degrees or medium heat');
window.alert('Step 6: Add one tbsp of olive oil to the skillet');
window.alert('Step 7: When the olive is hot add tomato and onion slices and saute lightly for 3 mins');
window.alert('Step 8: Place tomato and onion slices on the plate');
window.alert('Step 9: Return the skillet to the heat source at 350 degrees F');
window.alert('Step 10: Set bacon strips in the skillet');
window.alert('Step 11: Fry bacon on one side for 3 mins or until bacon strips begin to curl');
window.alert('Step 12: Use the fork to flip the bacon strips and fry on the other side for an additional 3 mins');
window.alert('Step 13: Place the fried bacon on the plate with sliced onions and tomatoes');
window.alert('Step 14: Drain the bacon grease from skillet into the heat resistant cup and set skillet to the side');
window.alert('Step 15: Turn off heat source');
window.alert('Step 16: Take 2 slices of bread and set them side by side on the plate');
window.alert('Step 17: Place 1 provolone cheese slice on each slice of bread');
window.alert('Step 18: Place tomato and onion slices on one slice of bread');
window.alert('Step 19: Place bacon strips on top on tomato and onion slice');
window.alert('Step 20: Place sliced turkey breast on top of bacon strips');
window.alert('Step 21: Use a butter knife to add mayo to the slice of bread with only provolone cheese');
window.alert('Step 22: Place the other slice of bread with mayo and provolone cheese on top of the sliced turkey breast');
window.alert('Step 23: Use the other butter knife to add butter to the outside of each slice of bread');
window.alert('Step 24: Return the skillet to the stove top and reheat to 350 degrees F');
window.alert('Step 25: Use the spatula to place the sandwich in the skillet');
window.alert('Step 26: Grill the sandwich for 2 minutes on one side');
window.alert('Step 27: Use the spatula to flip the sandwich');
window.alert('Step 28: Grill the sandwich for an additional 2 minutes');
window.alert('Step 29: Turn off heat source');
window.alert('Step 30: Use the spatula to remove the sandwich from the skillet');
window.alert('Step 31: Place the sandwich on the plate and use the vegetable knife to cut the sandwich in half');
window.alert('Step 32: Enjoy!');