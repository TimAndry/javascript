//This program calculates high, low, and average scores asynchronously

//runs the program

main();

function main(){
  setUp();
}

//This function reads the file, parses the data, and computes high, low, and average score
function setUp(){
  
  var fs = require('fs');
  var readline = require('readline');
  var stream = require('stream');
  //added error handling
  var instream = fs.createReadStream('assignment14.txt', 'utf8', function(error){
    if(error){
      console.error(error);
    }
  });
  var outstream = new stream;
  var rl = readline.createInterface(instream, outstream);
  var arr = [];
  
  //parses data into an array line by line
  rl.on('line', function(line) {
    // process line here
    arr.push(line);
  });
  
  //after data has been entered into the array non-integer values are removed and scores are calculated
  rl.on('close', function() {
    // do something on finish here
    
    for(var i = 0; i <= arr.length-1; i++){
      arr[i] = parseFloat(arr[i].replace(/\D/g,''));
    }
    
    console.log("high score: " + Math.max.apply(null, arr));
    console.log("low score: " + Math.min.apply(null, arr));
    var total=0;
    for(var j=0; j<=arr.length-1; j++){
      total = total + arr[j];
    }
    console.log("average score: " + total/arr.length );
  });
}
