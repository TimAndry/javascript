//This is a program that displays high, low, and average scores based on input from a file
main();

// main function to run the program
function main(){
    var lines = setUp();
    parseLines(lines);
    calcScores(lines);
}

//reads file content synchronously and parses it into an array
function setUp(){
    var fs = require('fs');
    var content = fs.readFileSync('assignment14.txt', "utf8");
    var lines = content.split("\n");
    return lines;
}

//removes all non integers from the array values and replaces them with empty space
function parseLines(lines){
    for(var i = 0; i <= lines.length-1; i++){
        lines[i] = parseFloat(lines[i].replace(/\D/g,''));
    }
    return lines;
}

//changes array values into integer variables and calculates scores
function calcScores(lines){
    console.log("high score: " + Math.max.apply(null, lines));
    console.log("low score: " + Math.min.apply(null, lines));
    var total=0;
    for(var i=0; i<=lines.length-1; i++){
        total = total + lines[i];
    }
    console.log("average score: " + total/lines.length );
}
