#This is a python program that outputs a persons age in months, days, hours, and seconds.

personAge = None
ageMonths = None
ageDays = None
ageHours = None
ageSeconds = None
text = None

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)


personAge = text_prompt('Please enter Your Age')
ageMonths = personAge * 12
ageDays = personAge * 365
ageHours = ageDays * 24
ageSeconds = ageHours * 3600
print(''.join([str(x) for x in ['Your age written in months, days, hours, or seconds is ', ageMonths, ' months, ', ageDays, ' days, ', ageHours, ' hours, or ', ageSeconds, ' seconds.']]))