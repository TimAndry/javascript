//This is a javascript program that outputs a person's age in months, days, hours, and seconds.

var personAge, ageMonths, ageDays, ageHours, ageSeconds, text;


personAge = window.prompt('Please enter Your Age');
ageMonths = personAge * 12;
ageDays = personAge * 365;
ageHours = ageDays * 24;
ageSeconds = ageHours * 3600;
window.alert(['Your age written in months, days, hours, or seconds is ',ageMonths,' months, ',ageDays,' days, ',ageHours,' hours, or ',ageSeconds,' seconds.'].join(''));