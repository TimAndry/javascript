//This is a program that calculates and displays the day of the week in which a person was born
main();

//main function to run the program
function main(){
    var birthMonth = getMonth();
    var birthDay = getDay();
    var birthYear = getYear();
    var weekDay = calcWeekday(birthMonth, birthDay, birthYear);
    var weekDays = dayArray();
    displayDay(weekDay, weekDays, birthDay, birthMonth, birthYear);
}

//gets user birth month
function getMonth(){
    var birthMonth = parseInt(input("What month were you born in? enter 1-12"),10);
    return birthMonth;
}

//gets user birth day
function getDay(){
    var birthDay = parseInt(input("What Day were you born on?"),10);
    return birthDay;
}

//gets user birth year
function getYear(){
    var birthYear =parseInt(input("What year four digit year were you born in? i.e. 1987"),10);
    return birthYear;
}

//calculates which day of the week the user was born on using Zeller's congruence
function calcWeekday(birthMonth, birthDay, birthYear){
    var q = birthDay;
    var m = birthMonth;
    var Y = birthYear;
    var h = weekDay;
    if(m == 1){
      h = (q + Math.floor(Math.floor((13 * (13 +1)))/5) + (Y-1) + Math.floor((Y-1)/4) - Math.floor((Y-1)/100) + Math.floor((Y-1)/400))%7;
    }else if(m == 2){
      h = (q + Math.floor(Math.floor((13 * (14 +1)))/5) + (Y-1) + Math.floor((Y-1)/4) - Math.floor((Y-1)/100) + Math.floor((Y-1)/400))%7;
    }else{
    h = (q + Math.floor(Math.floor((13 * (m +1)))/5) + Y + Math.floor(Y/4) - Math.floor(Y/100) + Math.floor(Y/400))%7;}
    var weekDay = h;
    return weekDay;
}

//creates an array to associate the calculated value with the string name
function dayArray(){
  var weekDays = ["Saturday", "Sunday", "Monday", "tuesday", "Wednesday", "Thursday", "Friday"];
  return weekDays;
}

//displays the day of the week the user was born on
function displayDay(weekDay, weekDays, birthDay, birthMonth, birthYear){
    var dayOfWeek = "You were born on " + weekDays[weekDay] + " " + birthMonth + "/" +birthDay + "/" + birthYear;
    output(dayOfWeek);
}










function input(text) {
    if (typeof console === 'object') {
        return prompt(text)
    }
    else {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) {
    if (typeof console === 'object') {
        console.log(text);
    } 
    else if (typeof document === 'object') {
        document.write(text);
    } 
    else {
        print(text);
    }
}