//This program is used to calculate a leap year
main();

//main function to run the program
function main(){
    var userYear = getYear();
    var leapResult = calcLeap(userYear);
    dispLeap(leapResult);
    var months = createMonthArray();
    var numDays = createDaysArray();
    var leapDays = createLeapArray();
    var normalMonth = makeRegMonth(months, numDays);
    var leapMonth = makeLeapMonth(months, leapDays);
    results(userYear, normalMonth, leapMonth);
}

//gets the requested year
function getYear(){
    var userYear = parseFloat(input("Please enter the year to find out if the year is a leap year"));
    return userYear;
}

//calculates whether or not the year was a leap year
function calcLeap(userYear){
    var leapResult;
    if (userYear%4 === 0 && userYear%100 !==0){
            leapResult = userYear + " is a Leap year";
        }else if (userYear%4 === 0 && userYear%100 === 0 && userYear %400 === 0){
          leapResult =  userYear + " is a Leap year" + "\n"+ "\n";
        }else{
          leapResult = userYear + " is NOT a leap year"+ "\n"+ "\n";
        }
        return leapResult;
    }
    
//displays the result of the leap year calculation    
function dispLeap(leapResult){
  output(leapResult);
}
    
//creates an array of months of the year    
function createMonthArray(){
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return months;
}

//parallel array with number of corresponding days for each month for non leap year
function createDaysArray(){
    var numDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    return numDays;
}

//parallel array with number of corresponding days for each month for non leap year
function createLeapArray(){
    var leapDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    return leapDays;
}

//makes an array from the months array and non leap year days
function makeRegMonth(months, numDays){
    var normalMonth = [];
    for (var i = 0; i < 12; i++){
        normalMonth[i] = months[i] + " has " + numDays[i] + " days";
    }
    return normalMonth;
}

//makes an array from the months array and leap year days
function makeLeapMonth(months, leapDays){
    var leapMonth = [];
    for (var i = 0; i < 12; i++){
        leapMonth[i] = months[i] + " has " + leapDays[i] + " days";
    }
    return leapMonth;
}

//displays the normal year month and day
function dispNormal(userMonth, normalMonth){
    var showMonth = normalMonth[userMonth - 1];
    output(showMonth);
}

//displays the leap year month and day
function dispLeapYear(userMonth, leapMonth){
    var showLeap = leapMonth[userMonth - 1];
    output(showLeap);
}

//loop to get the month and day results for each selected month until the user enters a non month number
function results(userYear, normalMonth, leapMonth){
  var selection = true;
  while(selection === true){
    var userMonth = input("Enter a month number 1-12 to see how many day are in it");
    if (userMonth>12 || userMonth <1){
      selection = false;
    }else if (userYear%4 === 0 && userYear%100 !==0){
            dispLeapYear(userMonth, leapMonth);
            }else if (userYear%4 === 0 && userYear%100 === 0 && userYear %400 === 0){
            dispLeapYear(userMonth, leapMonth);
            }else{
            dispNormal(userMonth, normalMonth);
      }
    }
  }  



function input(text)
{
    if (typeof console === 'object') 
    {
        return prompt(text);
    }
    else 
    {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) 
{
    if (typeof console === 'object') 
    {
        console.log(text);
    } 
    else if (typeof document === 'object') 
    {
        document.write(text);
    } 
    else 
    {
        print(text);
    }
}
