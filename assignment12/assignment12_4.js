//This is a program to simulate the Monty Hall problem

main();

//main function to execute the program
function main(){
    var doorArray = createDoors();
    pickDoor(doorArray);
    //showPrize(randomChoice);
    //showPrize(randomChoice, doorArray);
}

//This function creates the door array and randomizes the entries
function createDoors(){
   var doorArray = [0, 0, 1];
   doorArray.sort(function(a, b){return 0.5 - Math.random()});
   return doorArray;
}

//This function chooses a random door and displays whether a prize was won or not
function pickDoor(doorArray){
    var count = 0;
    //Added a loop to run the scenario multiple times to check percentages
    for(var i=0; i<=100; i++){
        //selects a random choice from the array
        var randomChoice = doorArray[Math.floor(Math.random() * 3)];
        if(randomChoice == 1){
            output("congratulations, you won the car");
            count = count+1;
        //When a goat door is opened the door is removed and the choice is switched
        }else if(randomChoice == 0){
            doorArray.splice(randomChoice, randomChoice);
            randomChoice = doorArray[Math.floor(Math.random() * 2)];
            if(randomChoice == 1){
                output("congratulations, you won the swapped car");
                count = count+1;
            }else if(randomChoice == 0){
                output("sorry, but you got a goat");
            }
        }
    }
    //outputs the count of the number of wins
    output(count + " out of 100 times");
}

//This is the program without the opportunity to switch choices

/*function showPrize(randomChoice){
    if(randomChoice == 1){
        output("congratulations, you won the car");
    }else if(randomChoice == 0){
        output("sorry, but you got a goat");
    }
}*/

/*function showPrize(randomChoice, doorArray){
    if(randomChoice == 1){
        output("congratulations, you won the car");
    }else if(randomChoice == 0){
        doorArray.splice(randomChoice, randomChoice);
        randomChoice = doorArray[Math.floor(Math.random() * 2)];
        if(randomChoice == 1){
            output("congratulations, you won the car");
        }else if(randomChoice == 0){
            output("sorry, but you got a goat");
        }
    }
}*/



function input(text) {
    if (typeof console === 'object') {
        return prompt(text)
    }
    else {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) {
    if (typeof console === 'object') {
        console.log(text);
    } 
    else if (typeof document === 'object') {
        document.write(text);
    } 
    else {
        print(text);
    }
}