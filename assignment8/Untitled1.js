//program to ask for a distance in miles and convert it into a standard or metric measurement

main();

function main(){
    var distanceMiles = getDistance();
    var measureChoice = chooseMethod();
    measureChoice;
    var standardMeasure = chooseMeasure();
    var metricMeasure = chooseMetric();
    if(chooseMethod == 1){
        standardMeasure;
            if(chooseMeasure == 1){
                var yards = calcYards(distanceMiles);
                var resultYards = showYards(yards);
                yards;
                resultYards;
            }else if(chooseMeasure == 2){
                var feet = calcFeet(distanceMiles);
                var resultFeet = showFeet(feet);
                feet;
                resultFeet;
            }else if(chooseMeasure == 3){
                var inches = calcInches(distanceMiles);
                var resultsInches = showInches(inches);
                inches;
                resultsInches;
            }else{
                chooseMeasure();
            }
    }else if(chooseMethod == 2){
        metricMeasure;
            if(chooseMetric == 1){
                var kilometers = calcKilos(distanceMiles);
                var resultKilos = showKilos(kilometers);
                kilometers;
                resultKilos;
            }else if(chooseMetric == 2){
                var meters = calcMeters(distanceMiles);
                var resultMeters = showMeters(meters);
                meters;
                resultMeters;
            }else if(chooseMetric == 3){
                var centimeters = calcCentimeters(distanceMiles);
                var resultCentimeters = showCentimeters(centimeters);
                centimeters;
                resultCentimeters;
            }else{
                chooseMetric();
            }
    }else{
        chooseMethod();
    }
}

function getDistance(){
    var distanceMiles = parseFloat(window.prompt("Enter the distance in miles you wish to convert"));
    return distanceMiles;
}

function chooseMethod(){
    var measureChoice = parseFloat(window.prompt("Choose measurement method from the meny" + "\n" + "\n" + "1. US Standard" + "\n" + "2. Metric"));
    return measureChoice;
}

function chooseMeasure(){
    var standardMeasure = parseFloat(window.prompt("What would you like to convert it to. Select a choince from the menu" + "\n" + "\n" + "1. Yards" + "\n" + "2. Feet" + "\n" + "3. Inches"));
    return standardMeasure;
}

function chooseMetric(){
    var metricMeasure = parseFloat(window.prompt("What would you like to convert it to. Select a choince from the menu" + "\n" + "\n" + "1. Kilometers" + "\n" + "2. Meters" + "\n" + "3. Centimeters"));
    return metricMeasure;
}

function calcYards(distanceMiles){
    var yards = (distanceMiles * 5280) / 3;
    return yards;
}

function calcFeet(distanceMiles){
    var feet = distanceMiles * 5280;
    return feet;
}

function calcInches(distanceMiles){
    var inches = distanceMiles * 5280 * 12;
    return inches;
}

function calcKilos(distanceMiles){
    var kilometers = distanceMiles / 1.6;
    return kilometers;
}

function calcMeters(distanceMiles){
    var meters = (distanceMiles * 1000) / 1.6;
    return meters;
}

function calcCentimeters(distanceMiles){
    var centimeters = (distanceMiles * 1000 * 100) / 1.6;
    return centimeters;
}

function showYards(yards, distanceMiles){
    var resultYards = "The number of yards in " + distanceMiles + " is " + yards + " yards";
    window.alert(resultYards);    
}

function showFeet(feet, distanceMiles){
    var resultFeet = "The number of feet in " + distanceMiles + " is " + feet + " feet";
    window.alert(resultFeet);    
}

function showInches(inches, distanceMiles){
    var resultInches = "The number of inches in " + distanceMiles + " is " + inches + " inches";
    window.alert(resultInches);    
}

function showKilos(kilometers, distanceMiles){
    var resultKilos = "The number of kilometers in " + distanceMiles + " is " + kilometers + " kilometers";
    window.alert(resultKilos);    
}

function showMeters(meters, distanceMiles){
    var resultMeters = "The number of meters in " + distanceMiles + " is " + meters + " meters";
    window.alert(resultMeters);    
}

function showCentimeters(centimeters, distanceMiles){
    var resultCentimeters = "The number of centimeters in " + distanceMiles + " is " + centimeters + " centimeters";
    window.alert(resultCentimeters);    
}


