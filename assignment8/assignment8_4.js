//This is a program that converts miles into smaller standard or metric units.

main();

//main function that allows users to select whether miles will be broken down into US standard units or metric units
function main(){
    pickChoice();
}


//function that selects metric or standard menu
function pickChoice(){
    var measureChoice;
    measureChoice = parseFloat(window.prompt("Choose measurement method from the menu" + "\n" + "\n" + "1. US Standard" + "\n" + "2. Metric"));
    if (measureChoice == 1){
        pickStandard();
    }else if(measureChoice == 2){
        pickMetric();
    }else{
        parseFloat(window.prompt("Choose measurement method from the menu" + "\n" + "\n" + "1. US Standard" + "\n" + "2. Metric"));
    }
}

//function that allows user to pick a measurement from a US standard set of measurements
function pickStandard(){
    var standardChoice;
    standardChoice = parseFloat(window.prompt("What would you like to convert it to. Select a choice from the menu" + "\n" + "\n" + "1. Miles to Yards" + "\n" + "2. Miles to Feet" + "\n" + "3. Miles to Inches"));
    switch (standardChoice) {
        case 1:
            getYards();
            break;
        case 2:
            getFeet();
            break;
        case 3:
            getInches();
            break;
        default:
            parseFloat(window.prompt("What would you like to convert miles to. Select a choince from the menu" + "\n" + "\n" + "1. Yards" + "\n" + "2. Feet" + "\n" + "3. Inches"));
    }
}

//function that allows user to pick a measurement from a metric set of measurements
function pickMetric(){
    var metricChoice;
    metricChoice = parseFloat(window.prompt("What would you like to convert it to. Select a choice from the menu" + "\n" + "\n" + "1. Miles to kilometers" + "\n" + "2. Miles to meters" + "\n" + "3. Miles to centimeters"));
    switch (metricChoice) {
        case 1:
            getKilos();
            break;
        case 2:
            getMeters();
            break;
        case 3:
            getCentimeters();
            break;
        default:
            parseFloat(window.prompt("What would you like to convert miles to. Select a choince from the menu" + "\n" + "\n" + "1. Kilometers" + "\n" + "2. Meters" + "\n" + "3. Centimeters"));
    }
}

//function that converts miles to yards
function getYards(){
    var miles = enterMiles();
    var yards = calcYards(miles);
    showYards(yards, miles);
}

//function that converts miles to feet
function getFeet(){
    var miles = enterMiles();
    var feet = calcFeet(miles);
    showFeet(feet, miles);
}

//function that converts miles to inches
function getInches(){
    var miles = enterMiles();
    var inches = calcInches(miles);
    showInches(inches, miles);
}

//function that converts miles to kilos
function getKilos(){
    var miles = enterMiles();
    var kilos = calcKilos(miles);
    showKilos(kilos, miles);
}

//function that converts miles to meters
function getMeters(){
    var miles = enterMiles();
    var meters = calcMeters(miles);
    showMeters(meters, miles);
}

//function that converts miles to centimeters
function getCentimeters(){
    var miles = enterMiles();
    var centimeters = calcCentimeters(miles);
    showCentimeters(centimeters, miles);
}

//gathers miles from user
function enterMiles(){
    var miles = parseFloat(window.prompt("Enter the distance in miles you wish to convert"));
    return miles;
}

//calculate the number of yards
function calcYards(miles){
    var yards = miles * 1760;
    return yards;
}

//shows the number of yards
function showYards(yards, miles){
    var resultYards = "The number of yards in " + miles + " miles is " + yards + " yards";
    window.alert(resultYards);
}

//calculates the number of feet
function calcFeet(miles){
    var feet = miles * 5280;
    return feet;
}

//shows the number of feet
function showFeet(feet, miles){
    var resultFeet = "The number of feet in " + miles + " miles is " + feet + " feet";
    window.alert(resultFeet);
}

//calculates the number of inches
function calcInches(miles){
    var inches = miles * 5280 * 12;
    return inches;
}

//displays the number of inches
function showInches(inches, miles){
    var resultInches = "The number of inches in " + miles + " miles is " + inches + " inches";
    window.alert(resultInches);
}

//calculates the number of kilos
function calcKilos(miles){
    var kilos = miles * 1.6;
    return kilos;
}

//displays the number of kilos
function showKilos(kilos, miles){
    var resultKilos = "The number of kilometers in " + miles + " miles is " + kilos + " kilometers";
    window.alert(resultKilos);
}

//calculates the number of meters
function calcMeters(miles){
    var meters = miles * 1.6 * 1000;
    return meters;
}

//displays the number of meters
function showMeters(meters, miles){
    var resultMeters = "The number of meters in " + miles + " miles is " + meters + " meters";
    window.alert(resultMeters);
}

//calculates the number of centimeters
function calcCentimeters(miles){
    var centimeters = miles * 1.6 * 1000 * 100;
    return centimeters;
}

//displays the number of centimeters
function showCentimeters(centimeters, miles){
    var resultCentimeters = "The number of centimeters in " + miles + " miles is " + centimeters + " centimeters";
    window.alert(resultCentimeters);
}