//this is a program that displays a persons age in months, days, hours, or seconds

//main function to convert years into a months, days, hours, or seconds.
function main(){
    var yearsOld = getYears();
    var ageRequest = getChoice();
    //If statement to determine the calculation.
    if (ageRequest == 1){
        var ageMonths = calcMonths(yearsOld);
        var resultMonths = showMonths(ageMonths);
        ageMonths;
        resultMonths;
    }else if(ageRequest == 2){
        var ageDays = calcDays(yearsOld);
        var resultDays = showDays(ageDays);
        ageDays;
        resultDays;
    }else if(ageRequest == 3){
        var ageHours = calcHours(yearsOld);
        var resultHours = showHours(ageHours);
        ageHours;
        resultHours;
    }else if(ageRequest === 4){
        var ageSeconds = calcSeconds(yearsOld);
        var resultSeconds = showSeconds(ageSeconds);
        calcSeconds(yearsOld);
        resultSeconds;
    }else {
        window.prompt("You must choose a NUMBER from the menu" + "\n" + "\n" + "1. Months" + "\n" + "2. Days" + "\n" + "3. Hours" + "\n" + "4. Seconds");
    }
}

//Function to get users age
function getYears(){
    var yearsOld = parseFloat(window.prompt("How old are you in years?"));
    return yearsOld;
}

//Function to select which value to convert years into
function getChoice(){
    var ageRequest = parseFloat(window.prompt("Enter a number from the menu to calculate your age in one of the following values" + "\n" + "\n" + "1. Months" + "\n" + "2. Days" + "\n" + "3. Hours" + "\n" + "4. Seconds"));
    return ageRequest;
}

//Function to calculate the number of months from years
function calcMonths(yearsOld){
    var ageMonths = yearsOld * 12;
    return ageMonths;
}

//Function to calculate the number of days from years
function calcDays(yearsOld){
    var ageDays = yearsOld * 365;
    return ageDays;
}

//Function to calculate the number of hours from years
function calcHours(yearsOld){
    var ageHours = yearsOld * 365 * 24;
    return ageHours;
}

//Function to calculate the number of seconds from years
function calcSeconds(yearsOld){
    var ageSeconds = yearsOld * 365 * 24 * 60 * 60;
    return ageSeconds;
}

//Displays age in months
function showMonths(ageMonths){
    var resultMonths = "Your age in months is " + ageMonths + " months old";
    window.alert(resultMonths);
}

//Displays age in days
function showDays(ageDays){
    var resultDays = "Your age in days is " + ageDays + " days old";
    window.alert(resultDays);
}

//Displays age in hours
function showHours(ageHours){
    var resultHours = "Your age in hours is " + ageHours + " hours old";
    window.alert(resultHours);
}

//Displays age in seconds
function showSeconds(ageSeconds){
    var resultSeconds = "Your age in seconds is " + ageSeconds + " seconds old";
    window.alert(resultSeconds);
}

main();