//This is a program that lets a user select a shape and input values to calculate the area of the shape
main();

//main function
function main(){
    getChoice();
}

//function that lets user select which shape to use
function getChoice(){
    var choice = parseFloat(window.prompt("Which shape would you like to know the area of? enter selection from the menu" + "\n" + "\n" + "1. Triangle" + "\n" + "2. Rectangle" + "\n" + "3. Trapezoid" + "\n" + "4. Ellipse" + "\n" + "5. Square" + "\n" + "6. Parallelogram" + "\n" + "7. Circle"+ "\n" + "8. Sector"));
    if (choice == 1){
        getTriangle();
    }else if (choice == 2){
        getRectangle();
    }else if (choice == 3){
        getTrapezoid();
    }else if (choice == 4){
        getEllipse();
    }else if (choice == 5){
        getSquare();
    }else if (choice == 6){
        getParallelogram();
    }else if (choice == 7){
        getCircle();
    }else if (choice == 8){
        getSector();
    }else{
        choice = parseFloat(window.prompt("Which shape would you like to know the area of? enter selection from the menu"));
    }
}

//function for finding the area of a triangle
function getTriangle(){
    var height = getHeight();
    var base = getBase();
    var areaTriangle = calcTriangle(base, height);
    showTriangle(areaTriangle);
}

//function for finding the area of a rectangle
function getRectangle(){
    var height = getHeight();
    var base = getBase();
    var areaRectangle = calcRectangle(base, height);
    showRectangle(areaRectangle);
}

//function for finding the area of a trapezoid
function getTrapezoid(){
    var top = getTopLength();
    var height = getHeight();
    var base = getBase();
    var areaTrapezoid = calcTrapezoid(top, base, height);
    showTrapezoid(areaTrapezoid);
}

//function for finding the area of a ellipse
function getEllipse(){
    var horz = getHorzRad();
    var vert = getVertRad();
    var areaEllipse = calcEllipse(horz, vert);
    showEllipse(areaEllipse);
}

//function for finding the area of a square
function getSquare(){
    var base = getBase();
    var areaSquare = calcSquare(base);
    showSquare(areaSquare);
}

//function for finding the area of a parallelogram
function getParallelogram(){
    var base = getBase();
    var height = getHeight();
    var areaParallelogram = calcParallelogram(base, height);
    showParallelogram(areaParallelogram);
}

//function for finding the area of a circle
function getCircle(){
    var radius = getRadius();
    var areaCircle = calcCircle(radius);
    showCircle(areaCircle);
}

//function for finding the area of a sector
function getSector(){
    var radius = getRadius();
    var angle = getAngle();
    var areaSector = calcSector(radius, angle);
    showSector(areaSector);
}


//function that gets a height input from a user
function getHeight(){
    var height = parseFloat(window.prompt("Enter the height"));
    return height;
}

//function that gets a base value input from a user
function getBase(){
    var base = parseFloat(window.prompt("Enter the base length"));
    return base;
}

//function that gets a top length value from a user
function getTopLength(){
    var top = parseFloat(window.prompt("Enter the top length"));
    return top;
}

//function that gets a horizontal radius input from a user
function getHorzRad(){
    var horz = parseFloat(window.prompt("Enter the horizontal radius"));
    return horz;
}

//function that gets a vertical radius input from a user
function getVertRad(){
    var vert = parseFloat(window.prompt("Enter the vertical radius"));
    return vert;
}

//function that gets a radius input from a user
function getRadius(){
    var radius = parseFloat(window.prompt("Enter the radius"));
    return radius;
}

//function that gets an angle input from a user
function getAngle(){
    var angle = parseFloat(window.prompt("Enter the angle in RADIANS"));
    return angle;
}

//function that calculates the area of a triangle
function calcTriangle(base, height){
    var areaTriangle = base * height * 0.5;
    return areaTriangle;
}

//function that calculates the area of a rectangle
function calcRectangle(base, height){
    var areaRectangle = base * height;
    return areaRectangle;
}

//function that calculates the area of a trapezoid
function calcTrapezoid(top, base, height){
    var areaTrapezoid = ((top + base)/2) * height;
    return areaTrapezoid;
}

//function that calculates the area of an ellipse
function calcEllipse(horz, vert){
    var areaEllipse = Math.PI * horz * vert;
    return areaEllipse;
}

//function that calculates the area of a square
function calcSquare(base){
    var areaSquare = base^2;
    return areaSquare;
}

//function that calculates the area of a parallelogram
function calcParallelogram(base, height){
    var areaParallelogram = base * height;
    return areaParallelogram;
}

//function that calculates the area of a circle
function calcCircle(radius){
    var areaCircle = Math.PI * radius^2;
    return areaCircle;
}

//function that calculates the area of a sector
function calcSector(radius, angle){
    var areaSector = ((radius^2)/2) * angle;
    return areaSector;
}

//function that displays the area of the triangle
function showTriangle(areaTriangle){
    var resultsTriangle = "The area of the triangle is " + areaTriangle;
    window.alert(resultsTriangle);
}

//function that displays the area of the rectangle
function showRectangle(areaRectangle){
    var resultsRectangle = "The area of the rectangle is " + areaRectangle;
    window.alert(resultsRectangle);
}

//function that displays the area of the trapezoid
function showTrapezoid(areaTrapezoid){
    var resultsTrapezoid = "The area of the trapezoid is " + areaTrapezoid;
    window.alert(resultsTrapezoid);
}

//function that displays the area of the ellipse
function showEllipse(areaEllipse){
    var resultsEllipse = "The area of the Ellipse is " + areaEllipse;
    window.alert(resultsEllipse);
}

//function that displays the area of the square
function showSquare(areaSquare){
    var resultsSquare = "The area of the square is " + areaSquare;
    window.alert(resultsSquare);
}

//function that displays the area of the parallelogram
function showParallelogram(areaParallelogram){
    var resultsParallel = "The area of the paralellogram is " + areaParallelogram;
    window.alert(resultsParallel);
}

//function that displays the area of the circle
function showCircle(areaCircle){
    var resultsCircle = "The area of the circle is " + areaCircle;
    window.alert(resultsCircle);
}

//function that displays the area of the sector
function showSector(areaSector){
    var resultsSector = "The area of the sector is " + areaSector;
    window.alert(resultsSector);
}