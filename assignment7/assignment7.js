var pay, rate, total, overtime, result, overResult;

/**
 * Calculates total pay from hours and rate
 */
function main() {
  pay = getPay();
  rate = getRate();
  if (rate <= 40) {
    total = calcRegularPay(pay, rate);
    displayRegular(total);
  } else {
    overtime = calcOvertimePay(pay, rate);
    displayOvertime(overtime);
  }
}

/**
 * Input for pay
 */
function getPay() {
  pay = parseFloat(window.prompt('Enter pay rate per hour'));
  return pay;
}

/**
 * Input for hours
 */
function getRate() {
  rate = parseFloat(window.prompt('Enter hours worked'));
  return rate;
}

/**
 * Calculates total pay without overtime
 */
function calcRegularPay(pay, rate) {
  total = pay * rate;
  return total;
}

/**
 * calculates total pay with overtime
 */
function calcOvertimePay(pay, rate) {
  overtime = (rate - 40) * pay * 1.5 + pay * 40;
  return overtime;
}

/**
 * Displays the results of pay without overtime
 */
function displayRegular(total) {
  result = 'You made $';
  result = String(result) + String(total);
  result = String(result) + String(' this pay period');
  window.alert(result);
}

/**
 * Displays the results of pay with overtime
 */
function displayOvertime(overtime) {
  overResult = 'you made $';
  overResult = String(overResult) + String(overtime);
  overResult = String(overResult) + String(' this pay period with overtime');
  window.alert(overResult);
}


main();