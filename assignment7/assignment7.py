pay = None
rate = None
total = None
overtime = None
result = None
overResult = None

"""Calculates total pay from hours and rate
"""
def main():
  global pay, rate, total, overtime, result, overResult
  pay = getPay()
  rate = getRate()
  if rate <= 40:
    total = calcRegularPay(pay, rate)
    displayRegular(total)
  else:
    overtime = calcOvertimePay(pay, rate)
    displayOvertime(overtime)

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)

"""Input for pay
"""
def getPay():
  global pay, rate, total, overtime, result, overResult
  pay = float(text_prompt('Enter pay rate per hour'))
  return pay

"""Input for hours
"""
def getRate():
  global pay, rate, total, overtime, result, overResult
  rate = float(text_prompt('Enter hours worked'))
  return rate

"""Calculates total pay without overtime
"""
def calcRegularPay(pay, rate):
  global total, overtime, result, overResult
  total = pay * rate
  return total

"""calculates total pay with overtime
"""
def calcOvertimePay(pay, rate):
  global total, overtime, result, overResult
  overtime = (rate - 40) * (pay * 1.5) + pay * 40
  return overtime

"""Displays the results of pay without overtime
"""
def displayRegular(total):
  global pay, rate, overtime, result, overResult
  result = 'You made $'
  result = str(result) + str(total)
  result = str(result) + str(' this pay period')
  print(result)

"""Displays the results of pay with overtime
"""
def displayOvertime(overtime):
  global pay, rate, total, result, overResult
  overResult = 'you made $'
  overResult = str(overResult) + str(overtime)
  overResult = str(overResult) + str(' this pay period with overtime')
  print(overResult)


main()