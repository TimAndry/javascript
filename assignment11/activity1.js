//This is a function that counts scores and displays the highest and lowest scores.

main();

function main(){
    var numScores = promptUser();
    var scoreArray = countScores(numScores);
    displayScores(scoreArray);
    var average = averageScores(scoreArray);
    displayLow(scoreArray);
    displayHigh(scoreArray);
    displayAverage(average);
}

function promptUser(){
    var numScores = parseFloat(input("How many scores do you wish to enter?"));
    return numScores;
}

function countScores(numScores){
    var scoreArray = [];
    var index;
    for(index = 0; index < numScores; index++){
        scoreArray.push(parseFloat(input("Enter a score")));
    }
    return scoreArray;
}

//Check function so far
function displayScores(scoreArray){
    output("The scores that you entered were " + scoreArray);
}

//calculate average of all scores
function averageScores(scoreArray){
    var totalScore = 0;
    for(var i = 0; i < scoreArray.length; i++){
        totalScore = totalScore + scoreArray[i];
    }
    var average = totalScore / scoreArray.length;
    return average;
}

//displays lowest score
function displayLow(scoreArray){
    var lowScore = "The lowest score you entered was " + Math.min.apply(null, scoreArray);
    output(lowScore);
}

//displays highest score
function displayHigh(scoreArray){
    var highScore = "The highest score you entered was " + Math.max.apply(null, scoreArray);
    output(highScore);
}

//displays the average of all scores
function displayAverage(average){
    var averageScore = "The average of all the scores you entered is " + average;
    output(averageScore);
}

function input(text) {
    if (typeof console === 'object') {
        return prompt(text)
    }
    else {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) {
    if (typeof console === 'object') {
        console.log(text);
    } 
    else if (typeof document === 'object') {
        document.write(text);
    } 
    else {
        print(text);
    }
}