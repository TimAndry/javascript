//this is a program that converts age in years to age in months, days, hours, and seconds

main();

//main function to calculate age at various time lengths
function main(){
    var ageYears = getAge();
    var ageMonths = calcMonths(ageYears);
    var ageDays = calcDays(ageYears);
    var ageHours = calcHours(ageDays);
    var ageSeconds = calcSeconds(ageHours);
    displayAges(ageYears, ageMonths, ageDays, ageHours, ageSeconds);
}

//function to get age of user
function getAge(){
    var ageYears = parseFloat(input("Please enter your age"));
    return ageYears;
}

//converts years to months
function calcMonths(ageYears){
    var ageMonths = ageYears * 12;
    return ageMonths;
}

//converts years to days
function calcDays(ageYears){
    var ageDays = ageYears * 365;
    return ageDays;
}

//converts days to hours
function calcHours(ageDays){
    var ageHours = ageDays * 24;
    return ageHours;
}

//converts hours to seconds
function calcSeconds(ageHours){
    var ageSeconds = ageHours * 60 * 60;
    return ageSeconds;
}

//displays the results of the calculations
function displayAges(ageYears, ageMonths, ageDays, ageHours, ageSeconds){
    var showAges = "age in years = " + ageYears + '\n' + "age in months = " + ageMonths + '\n' + "age in days = " + ageDays + '\n' + "age in hours = " + ageHours + '\n' + "age in Seconds = " + ageSeconds;
    output(showAges);
}

function input(text)
{
    if (typeof console === 'object') 
    {
        return prompt(text)
    }
    else 
    {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) 
{
    if (typeof console === 'object') 
    {
        console.log(text);
    } 
    else if (typeof document === 'object') 
    {
        document.write(text);
    } 
    else 
    {
        print(text);
    }
}