

//This is a program that displays the order of precedence used in programming

//main function to display order of operations of calculations
function main(){
  var firstNum = getFirst();
  var secondNum = getSecond();
  var thirdNum = getThird();
  var addNum = calcAddition(firstNum, secondNum, thirdNum);
  var subNum = calcSubtraction(firstNum, secondNum, thirdNum);
  var multNum = calcMultiplication(firstNum, secondNum, thirdNum);
  var divNum = calcDivision(firstNum, secondNum, thirdNum);
  var parenthNum = calcParenthesis(firstNum, secondNum, thirdNum);
  displayResultsAdd(firstNum, secondNum, thirdNum, addNum);
  displayResultsSub(firstNum, secondNum, thirdNum, subNum);
  displayResultsMult(firstNum, secondNum, thirdNum, multNum);
  displayResultsDiv(firstNum, secondNum, thirdNum, divNum);
  displayResultsParenth(firstNum, secondNum, thirdNum, parenthNum);
}

//function used to get the first number
function getFirst(){
    var firstNum = parseInt(window.prompt("Please enter your 1st number 1-9"));
    return firstNum;
}

//function used to get the second number
function getSecond(){
    var secondNum = parseInt(window.prompt("Please enter your 2nd number 1-9"));
    return secondNum;
}

//function used to get the third number
function getThird(){
     var thirdNum = parseInt(window.prompt("Please enter your 3rd number 1-9"));
     return thirdNum;
}

//function used to show order of operations for addition
function calcAddition(firstNum, secondNum, thirdNum){
    var addNum = firstNum + secondNum + thirdNum;
    return addNum;
}

//function used to show order of operations for subtraction
function calcSubtraction(firstNum, secondNum, thirdNum){
    var subNum = firstNum - secondNum - thirdNum;
    return subNum;
}

//function used to show order of operations for multiplication
function calcMultiplication(firstNum, secondNum, thirdNum){
    var multNum = firstNum + secondNum * thirdNum;
    return multNum;
}

//function used to show order of operations for division
function calcDivision(firstNum, secondNum, thirdNum){
    var divNum = firstNum + secondNum / thirdNum;
    return divNum;
}

//function used to show order of operations for parenthesis
function calcParenthesis(firstNum, secondNum, thirdNum){
    var parenthNum = (firstNum + secondNum) * thirdNum;
    return parenthNum;
}

//function used to display results for addition
function displayResultsAdd(firstNum, secondNum, thirdNum, addNum){
    var resultsAdd = firstNum + " + " + secondNum + " + " + thirdNum + " = " + addNum;
    window.alert(resultsAdd);
}

//function used to display results for subtraction
function displayResultsSub(firstNum, secondNum, thirdNum, subNum){
    var resultsSub = firstNum + " - " + secondNum + " - " + thirdNum + " = " + subNum;
    window.alert(resultsSub);
}

//function used to display results for multiplication
function displayResultsMult(firstNum, secondNum, thirdNum, multNum){
    var resultsMult = firstNum + " + " + secondNum + " * " + thirdNum + " = " + multNum;
    window.alert(resultsMult);
}

//function used to display results for division
function displayResultsDiv(firstNum, secondNum, thirdNum, divNum){
    var resultsDiv = firstNum + " + " + secondNum + " / " + thirdNum + " = " + divNum;
    window.alert(resultsDiv);
}

//function used to display results for parenthesis
function displayResultsParenth(firstNum, secondNum, thirdNum, parenthNum){
    var resultsParenth = "(" + firstNum + " + " + secondNum + ")" + " * " + thirdNum + " = " + parenthNum;
    window.alert(resultsParenth);
}

main();