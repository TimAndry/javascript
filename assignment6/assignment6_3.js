//This is a program that converts distance miles into distances in various standard and metric lengths

//main function

main();

function main(){
    var distMiles = getMiles();
    var distYards = calcYards(distMiles);
    var distFeet = calcFeet(distYards);
    var distInches = calcInches(distFeet);
    var distKilos = calcKilos(distMiles);
    var distMeters = calcMeters(distKilos);
    var distCentimeters = calcCentimeters(distMeters);
    displayStandard(distMiles, distYards, distFeet, distInches);
    displayMetric(distMiles, distKilos, distMeters, distCentimeters);
}

//function to get miles from user input
function getMiles(){
    var distMiles = parseFloat(input("Please input a distance in miles"));
    return distMiles;
}

//converts miles to yards
function calcYards(distMiles){
    var distYards = distMiles * 1760;
    return distYards;
}

//converts yards to feet
function calcFeet(distYards){
    var distFeet = distYards * 3;
    return distFeet;
}

//converts feet to inches
function calcInches(distFeet){
    var distInches = distFeet * 3;
    return distInches;
}

//converts miles to kilometers
function calcKilos(distMiles){
    var distKilos = distMiles * 1.609344;
    return distKilos;
}

//converts kilometers to meters
function calcMeters(distKilos){
    var distMeters = distKilos * 1000;
    return distMeters;
}

//converts meters to centimeters
function calcCentimeters(distMeters){
    var distCentimeters = distMeters * 100;
    return distCentimeters;
}

//displays results in standard lengths (yards, feet, inches)
function displayStandard(distMiles, distYards, distFeet, distInches){
    var standardResults = "distance in miles = " + distMiles + '\n' + "distance in yards = " + distYards + '\n' + "distance in feet = " + distFeet + '\n' + "distance in inches = " + distInches;
    output(standardResults);
}

//displays results in metric lengths (kilometers, meters, centimeters)
function displayMetric(distMiles, distKilos, distMeters, distCentimeters){
    var metricResults = "distance in miles = " + distMiles + '\n' + "distance in kilometers = " + distKilos + '\n' + "distance in meters = " + distMeters + '\n' + "distance in centimeters = " + distCentimeters;
    output(metricResults);
}

function input(text)
{
    if (typeof console === 'object') 
    {
        return prompt(text)
    }
    else 
    {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) 
{
    if (typeof console === 'object') 
    {
        console.log(text);
    } 
    else if (typeof document === 'object') 
    {
        document.write(text);
    } 
    else 
    {
        print(text);
    }
}
