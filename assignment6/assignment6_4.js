//program that uses Zeller's congruence to determine the day of the week that a person was born

//main function that that determines the day of the week
main();

function main(){
    var m = getMonth();
    var q = getDay();
    var y = getYear();
    var h = calcDay(m, q, y);
    displayResults(h);
}

function getMonth(){
   var m = parseFloat(input("What month were you born in?"));
   return m;
}

function getDay(){
    var q = parseFloat(input("What day of the month were you born on?"));
    return q;
}

function getYear(){
    var y = parseFloat(input("what 2 digit year were you born in?"));
    return y;
}

function calcDay(m, q, y){
    var h = (q + (26 *(m+1)/10) + y + (y/4) + 6 * (y/100) + (y/400));
    h = h % 7;
    return h;
}

function displayResults(h){
    var results = h;
    output(results);
}

function input(text)
{
    if (typeof console === 'object') 
    {
        return prompt(text)
    }
    else 
    {
        output(text);
        var isr = new java.io.InputStreamReader(java.lang.System.in); 
        var br = new java.io.BufferedReader(isr); 
        var line = br.readLine();
        return line.trim();
    }
}

function output(text) 
{
    if (typeof console === 'object') 
    {
        console.log(text);
    } 
    else if (typeof document === 'object') 
    {
        document.write(text);
    } 
    else 
    {
        print(text);
    }
}
    window.alert(results);


