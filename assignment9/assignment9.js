var scoreCount, totalScore, scoreAvg, averageResult, score;

/**
 * Main function used to get scores from a user and average them for a result
 */
function main() {
  scoreCount = countScores();
  totalScore = whileLoop(scoreCount);
  scoreAvg = averagScores(totalScore, scoreCount);
  printAverage(scoreAvg);
  totalScore = forLoop(scoreCount);
  scoreAvg = averagScores(totalScore, scoreCount);
  printAverage(scoreAvg);
}

/**
 * gets the number of scores that will be entered
 */
function countScores() {
  scoreCount = parseFloat(window.prompt('Enter the number of scores'));
  return scoreCount;
}

/**
 * gets individual scores and adds them together for a total using a while loop.
 */
function whileLoop(scoreCount) {
  totalScore = 0;
  while (scoreCount > 0) {
    score = parseFloat(window.prompt('Enter score'));
    totalScore = totalScore + score;
    scoreCount = (typeof scoreCount == 'number' ? scoreCount : 0) + -1;
  }
  return totalScore;
}

/**
 * gets individual scores and adds them together for a total using a for loop
 */
function forLoop(scoreCount) {
  totalScore = 0;
  var scoreCount_inc = 1;
  if (scoreCount > 1) {
    scoreCount_inc = -scoreCount_inc;
  }
  for (scoreCount = scoreCount; scoreCount_inc >= 0 ? scoreCount <= 1 : scoreCount >= 1; scoreCount += scoreCount_inc) {
    score = parseFloat(window.prompt('Enter score'));
    totalScore = totalScore + score;
  }
  return totalScore;
}

/**
 * Takes the sum of the score entries and divides
 * it by the number of entries to get the average.
 */
function averagScores(totalScore, scoreCount) {
  scoreAvg = totalScore / scoreCount;
  return scoreAvg;
}

/**
 * Prints the score averages.
 */
function printAverage(scoreAvg) {
  averageResult = String('The average of the scores you entered is ') + String(scoreAvg);
  window.alert(averageResult);
}


main();