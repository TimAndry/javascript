from numbers import Number

scoreCount = None
totalScore = None
scoreAvg = None
averageResult = None
score = None

"""Main function used to get scores from a user and average them for a result
"""
def main():
  global scoreCount, totalScore, scoreAvg, averageResult, score
  scoreCount = countScores()
  totalScore = whileLoop(scoreCount)
  scoreAvg = averagScores(totalScore, scoreCount)
  printAverage(scoreAvg)
  totalScore = forLoop(scoreCount)
  scoreAvg = averagScores(totalScore, scoreCount)
  printAverage(scoreAvg)

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)

"""gets the number of scores that will be entered
"""
def countScores():
  global scoreCount, totalScore, scoreAvg, averageResult, score
  scoreCount = float(text_prompt('Enter the number of scores'))
  return scoreCount

"""gets individual scores and adds them together for a total using a while loop.
"""
def whileLoop(scoreCount):
  global totalScore, scoreAvg, averageResult, score
  totalScore = 0
  while scoreCount > 0:
    score = float(text_prompt('Enter score'))
    totalScore = totalScore + score
    scoreCount = (scoreCount if isinstance(scoreCount, Number) else 0) + -1
  return totalScore

def upRange(start, stop, step):
  while start <= stop:
    yield start
    start += abs(step)

def downRange(start, stop, step):
  while start >= stop:
    yield start
    start -= abs(step)

"""gets individual scores and adds them together for a total using a for loop
"""
def forLoop(scoreCount):
  global totalScore, scoreAvg, averageResult, score
  totalScore = 0
  for scoreCount in (float(scoreCount) <= 1) and upRange(float(scoreCount), 1, -1) or downRange(float(scoreCount), 1, -1):
    score = float(text_prompt('Enter score'))
    totalScore = totalScore + score
  return totalScore

"""Takes the sum of the score entries and divides
it by the number of entries to get the average.
"""
def averagScores(totalScore, scoreCount):
  global scoreAvg, averageResult, score
  scoreAvg = totalScore / scoreCount
  return scoreAvg

"""Prints the score averages.
"""
def printAverage(scoreAvg):
  global scoreCount, totalScore, averageResult, score
  averageResult = str('The average of the scores you entered is ') + str(scoreAvg)
  print(averageResult)


main()