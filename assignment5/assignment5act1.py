#created a program to calculate pay rate

pay = None
rate = None
total = None
result = None

"""Calculates total pay from hours and rate
"""
def main():
  global pay, rate, total, result
  pay = getPay()
  rate = getRate()
  total = calcTotalPay(pay, rate)
  displayResults(total)

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)

"""Input for pay
"""
def getPay():
  global pay, rate, total, result
  pay = float(text_prompt('Enter pay rate per hour'))
  return pay

"""Input for hours
"""
def getRate():
  global pay, rate, total, result
  rate = float(text_prompt('Enter hours worked'))
  return rate

"""Calculates total pay
"""
def calcTotalPay(pay, rate):
  global total, result
  total = pay * rate
  return total

"""Displays the results
"""
def displayResults(total):
  global pay, rate, result
  result = 'You made $'
  result = str(result) + str(total)
  result = str(result) + str(' this pay period')
  print(result)


main()