

//created a program to calculate pay rate

var pay, rate, total, result;

/**
 * Calculates total pay from hours and rate
 */
function main() {
  pay = getPay();
  rate = getRate();
  total = calcTotalPay(pay, rate);
  displayResults(total);
}

/**
 * Input for pay
 */
function getPay() {
  pay = parseFloat(window.prompt('Enter pay rate per hour'));
  return pay;
}

/**
 * Input for hours
 */
function getRate() {
  rate = parseFloat(window.prompt('Enter hours worked'));
  return rate;
}

/**
 * Calculates total pay
 */
function calcTotalPay(pay, rate) {
  total = pay * rate;
  return total;
}

/**
 * Displays the results
 */
function displayResults(total) {
  result = 'You made $';
  result = String(result) + String(total);
  result = String(result) + String(' this pay period');
  window.alert(result);
}


main();